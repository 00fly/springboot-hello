package com.fly.hello.entity;

import java.util.List;

import lombok.Data;

@Data
public class Record
{
    private List<Article> list;
}
