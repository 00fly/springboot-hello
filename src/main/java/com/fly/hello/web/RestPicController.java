package com.fly.hello.web;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.fly.hello.common.RandomIndex;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * RestPicController
 * 
 * @author 00fly
 * @version [版本号, 2021年9月28日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Api(tags = "图片接口-api")
@RestController
@RequestMapping("/show")
@ConditionalOnWebApplication
public class RestPicController
{
    Resource[] resources = {};
    
    RandomIndex randomIndex;
    
    @PostConstruct
    private void init()
    {
        try
        {
            resources = new PathMatchingResourcePatternResolver().getResources(ResourceUtils.CLASSPATH_URL_PREFIX + "data/pic/**/*.jpg");
            randomIndex = new RandomIndex(resources.length);
            if (resources.length < 4)
            {
                log.error("############### 请在[resources/data/pic/]目录放入不少于4张jpg图片 ###############");
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @ApiOperation("图片")
    @GetMapping(value = {"/girl", "/pic"}, produces = MediaType.IMAGE_JPEG_VALUE)
    public StreamingResponseBody showPic()
    {
        return (output) -> {
            try (ByteArrayOutputStream os = new ByteArrayOutputStream())
            {
                ImageIO.write(createImage(), "jpg", os);
                output.write(os.toByteArray());
            }
        };
    }
    
    @ApiOperation("pic1")
    @GetMapping(value = "/pic1", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] showPic1()
        throws IOException
    {
        if (RandomUtils.nextBoolean())
        {
            try (ByteArrayOutputStream os = new ByteArrayOutputStream())
            {
                ImageIO.write(createImage(), "jpg", os);
                return os.toByteArray();
            }
        }
        // another process
        int index = randomIndex.getIndex();
        try (InputStream inputStream = resources[index].getInputStream())
        {
            return IOUtils.toByteArray(inputStream);
        }
    }
    
    @ApiOperation("pic2")
    @GetMapping(value = "/pic2", produces = MediaType.IMAGE_JPEG_VALUE)
    public void showPic2(HttpServletResponse response)
        throws IOException
    {
        // setHeader注解写法?
        response.setHeader("Cache-Control", "no-store, no-cache");
        if (RandomUtils.nextBoolean())
        {
            ImageIO.write(createImage(), "jpg", response.getOutputStream());
            return;
        }
        
        // another process
        int index = randomIndex.getIndex();
        try (InputStream inputStream = resources[index].getInputStream())
        {
            IOUtils.copy(inputStream, response.getOutputStream());
        }
    }
    
    @ApiOperation("文件下载-支持file")
    @GetMapping(value = "/down", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void down(HttpServletResponse response)
        throws IOException
    {
        if (ResourceUtils.isFileURL(ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX)))
        {
            List<Path> filePaths = new ArrayList<>();
            Stream.of(new ClassPathResource("data/pic").getFile().listFiles()).forEach(lib -> filePaths.add(lib.toPath()));
            
            // 压缩多个文件到zip文件中
            String filename = DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmss");
            response.setHeader("Content-Disposition", "attachment;filename=imgs_" + filename + ".zip");
            try (ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream()))
            {
                for (Path path : filePaths)
                {
                    try (InputStream inputStream = Files.newInputStream(path))
                    {
                        zipOutputStream.putNextEntry(new ZipEntry(path.getFileName().toString()));
                        StreamUtils.copy(inputStream, zipOutputStream);
                        zipOutputStream.flush();
                    }
                }
            }
        }
    }
    
    @ApiOperation("文件下载2-支持file、jar")
    @GetMapping(value = "/down2", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void down2(HttpServletResponse response)
        throws IOException
    {
        String filename = DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmss");
        response.setHeader("Content-Disposition", "attachment;filename=imgs_" + filename + ".zip");
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream()))
        {
            Stream.of(resources).forEach(r -> {
                try (InputStream inputStream = r.getInputStream())
                {
                    zipOutputStream.putNextEntry(new ZipEntry(r.getFilename()));
                    StreamUtils.copy(inputStream, zipOutputStream);
                    zipOutputStream.flush();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            });
        }
    }
    
    /**
     * createImage 生成图片
     * 
     * @return
     * @throws IOException
     * @see [类、类#方法、类#成员]
     */
    private BufferedImage createImage()
        throws IOException
    {
        if (resources.length < 4)
        {
            log.info("############### 请在[resources/data/pic/]目录放入不少于4张jpg图片 ###############");
            return new BufferedImage(400, 400, BufferedImage.TYPE_BYTE_GRAY);
        }
        // 取图片
        int index = randomIndex.getIndex();
        try (InputStream inputStream = resources[index].getInputStream())
        {
            return ImageIO.read(inputStream);
        }
    }
}