package com.fly.hello.web.async;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.fly.core.utils.jackson.JsonBeanUtils;
import com.fly.hello.entity.Article;
import com.fly.hello.service.DataService;
import com.fly.hello.service.SSEServer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "sse接口")
@RestController
@RequestMapping("/sse")
public class SSEController
{
    long init = -1L;
    
    @Autowired
    DataService dataService;
    
    @ApiOperation("初始化")
    @GetMapping("/connect/{userId}")
    public SseEmitter connect(@PathVariable String userId)
    {
        return SSEServer.connect();
    }
    
    @PostConstruct
    private void init()
    {
        log.info("Server-Sent Events start");
        new ScheduledThreadPoolExecutor(2).scheduleAtFixedRate(() -> {
            long now = (init + RandomUtils.nextInt(5, 10)) % 100;
            SSEServer.batchSendMessage(String.valueOf(now));
            if (now < init)
            {
                try
                {
                    // 随机选择2个，返回访问量小的
                    List<Article> articles = dataService.getArticles();
                    int length = articles.size();
                    Article article001 = articles.get(RandomUtils.nextInt(0, length));
                    Article article002 = articles.get(RandomUtils.nextInt(0, length));
                    SSEServer.batchSendMessage("json", JsonBeanUtils.beanToJson(article001.getViewCount() > article002.getViewCount() ? article002 : article001, false));
                }
                catch (IOException e)
                {
                }
            }
            init = now;
        }, 2000, 1000, TimeUnit.MILLISECONDS);
    }
    
    /**
     * 定时任务设置
     */
    // @Scheduled(fixedDelay = 1000)
    protected void sendMessage()
    {
        init = ++init % 101;
        SSEServer.batchSendMessage(String.valueOf(init));
    }
}