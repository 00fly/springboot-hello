package com.fly.hello.web.swagger;

import javax.validation.constraints.NotBlank;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Api(tags = "演示排序接口")
@Validated
@RestController
@RequestMapping("/sort")
public class SortedController
{
    @GetMapping("/first")
    @ApiOperationSupport(order = 100)
    @ApiOperation("001 @ApiImplicitParams演示")
    @ApiImplicitParams({@ApiImplicitParam(name = "key", value = "键", required = true), @ApiImplicitParam(name = "value", value = "值", required = true)})
    public JsonResult<?> first(String key, String value)
    {
        return JsonResult.success();
    }
    
    @GetMapping("/second")
    @ApiOperationSupport(order = 80)
    @ApiOperation("002 @ApiImplicitParam演示")
    @ApiImplicitParam(name = "key", value = "键", required = true)
    public JsonResult<?> second(String key)
    {
        return JsonResult.success();
    }
    
    @GetMapping("/third")
    @ApiOperationSupport(order = 60)
    @ApiOperation("003 @Parameter演示")
    @ApiImplicitParam(name = "key", value = "键,字符串", required = true)
    public JsonResult<?> third(String key)
    {
        return JsonResult.success();
    }
    
    @GetMapping("/fourth")
    @ApiOperationSupport(order = 40)
    @ApiOperation("004 @Parameter演示")
    @ApiImplicitParams({@ApiImplicitParam(name = "key", value = "键", required = true), @ApiImplicitParam(name = "value", value = "值", required = true)})
    public JsonResult<?> fourth(String key, String value)
    {
        return JsonResult.success();
    }
    
    @ApiOperation("参数校验")
    @GetMapping("/paramValid")
    public JsonResult<?> paramValid(@NotBlank(message = "name不能为空") String name)
    {
        return JsonResult.success();
    }
}
