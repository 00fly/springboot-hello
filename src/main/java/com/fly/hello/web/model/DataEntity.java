package com.fly.hello.web.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Range;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(description = "实体数据对象")
public class DataEntity
{
    @NotNull(message = "id不能为空")
    @ApiModelProperty(value = "主键", required = true, example = "1")
    private Long id;
    
    @NotBlank(message = "名称不能为空")
    @ApiModelProperty(value = "名称", required = true, example = "00fly.online")
    private String name;
    
    @NotBlank(message = "性别不能为空")
    @Pattern(regexp = "F|M", message = "性别不合法")
    @ApiModelProperty(value = "性别", required = true, example = "F", allowableValues = "F,M")
    private String sex;
    
    @NotNull(message = "年齡不能为空")
    @Min(value = 1, message = "年齡不能小于{value}")
    @Range(min = 1, max = 120, message = "年齡需要在{min}和{max}之间")
    @Digits(integer = 3, fraction = 0, message = "年齡只能在{integer}位整数范围内")
    @ApiModelProperty(value = "年齡", required = true, example = "15", allowableValues = "0,10,18,20")
    private Integer age;
    
    @NotNull(message = "薪资不能为空")
    @Min(value = 0, message = "薪资不能小于{value}")
    @ApiModelProperty(value = "薪资", required = true, example = "0.00")
    private Float salary;
    
    @NotNull(message = "游戏1积分不能为空")
    @Min(value = 0, message = "游戏1积分不能小于{value}")
    @ApiModelProperty(value = "游戏1积分", required = true, example = "0.00")
    private Double game1;
    
    @NotNull(message = "游戏2积分不能为空")
    @Min(value = 0, message = "游戏2积分不能小于{value}")
    @ApiModelProperty(value = "游戏2积分", required = true, example = "0.00")
    private BigDecimal game2;
    
    @ApiModelProperty(hidden = true)
    private Date creatTime;
}
