package com.fly.hello.service;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.fly.hello.entity.Article;
import com.fly.hello.entity.BlogData;

import lombok.extern.slf4j.Slf4j;

/**
 * DataService
 */
@Slf4j
@Service
public class DataService
{
    WebClient webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    
    /**
     * 获取url数据列表
     * 
     * @return
     */
    @Cacheable(cacheNames = "data", key = "'articles'", sync = true)
    public List<Article> getArticles()
    {
        log.info("★★★★★★★★ getData from webApi ★★★★★★★★");
        return webClient.get()
            .uri("https://00fly.online/upload/data.json")
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(BlogData.class)
            .block()
            .getData()
            .getList();
    }
}