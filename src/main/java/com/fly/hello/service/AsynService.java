package com.fly.hello.service;

/**
 * 
 * @author 00fly
 *
 */
public interface AsynService
{
    public void test(String traceId);
    
    public void call();
    
    public void test(String key, String taskId);
}
