package com.fly.core.utils.jackson;

import java.io.IOException;
import java.util.Properties;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * xml转换工具
 * 
 * @author 00fly
 *
 */
public class XmlUtils extends BaseJackson
{
    /**
     * xml转Json字符串
     * 
     * @param xmlContent
     * @return
     * @throws IOException
     */
    public static String xmlToJson(String xmlContent)
        throws IOException
    {
        JsonNode jsonNode = xmlMapper.readTree(xmlContent);
        return jsonNode.toPrettyString();
    }
    
    /**
     * xml转properties
     * 
     * @param xmlContent
     * @return
     * @throws IOException
     */
    public static Properties xmlToProperties(String xmlContent)
        throws IOException
    {
        JsonNode jsonNode = xmlMapper.readTree(xmlContent);
        return javaPropsMapper.writeValueAsProperties(jsonNode);
    }
    
    /**
     * xml转properties字符串
     * 
     * @param xmlContent
     * @return
     * @throws IOException
     */
    public static String xmlToPropText(String xmlContent)
        throws IOException
    {
        JsonNode jsonNode = xmlMapper.readTree(xmlContent);
        return javaPropsMapper.writeValueAsString(jsonNode);
    }
    
    /**
     * xml转yaml
     * 
     * @param xmlContent
     * @return
     * @throws IOException
     */
    public static String xmlToYaml(String xmlContent)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readTree(xmlContent);
        return yamlMapper.writeValueAsString(jsonNode);
    }
}
