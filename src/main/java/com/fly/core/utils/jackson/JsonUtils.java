package com.fly.core.utils.jackson;

import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * JsonUtils 转换工具
 * 
 * @author 00fly
 *
 */
public class JsonUtils extends BaseJackson
{
    /**
     * json转properties字符串
     * 
     * @param jsonText
     * @return
     * @throws IOException
     */
    public static String jsonToPropText(String jsonText)
        throws IOException
    {
        JsonNode jsonNode = jsonMapper.readTree(jsonText);
        return javaPropsMapper.writeValueAsString(jsonNode);
    }
    
    /**
     * Json转xml字符串
     * 
     * @param jsonText
     * @return
     * @throws IOException
     */
    public static String jsonToXml(String jsonText)
        throws IOException
    {
        JsonNode jsonNode = jsonMapper.readTree(jsonText);
        return xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
    }
    
    /**
     * json转yaml
     * 
     * @param jsonText
     * @return
     * @throws IOException
     */
    public static String jsonToYaml(String jsonText)
        throws IOException
    {
        JsonNode jsonNode = jsonMapper.readTree(jsonText);
        return yamlMapper.writeValueAsString(jsonNode);
    }
}
