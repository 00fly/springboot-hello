package com.fly.core.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class StatusAspect
{
    @Pointcut("execution(* com.fly.hello.service.impl.AsynServiceImpl.test(..))")
    public void pointcut()
    {
    }
    
    @Before("pointcut()")
    public void doBefore(JoinPoint joinPoint)
    {
        log.info("开始运行前, TaskId：{}", getTaskId(joinPoint));
    }
    
    @After("pointcut()")
    public void doAfter(JoinPoint joinPoint)
    {
        log.info("After advice:: TaskId：{}", getTaskId(joinPoint));
    }
    
    @AfterReturning("pointcut()")
    public void doAfterReturning(JoinPoint joinPoint)
    {
        log.info("正常结束, TaskId：{}", getTaskId(joinPoint));
    }
    
    @AfterThrowing("pointcut()")
    public void doAfterThrowing(JoinPoint joinPoint)
    {
        log.info("抛出异常, TaskId：{}", getTaskId(joinPoint));
    }
    
    private String getTaskId(JoinPoint joinPoint)
    {
        Object[] args = joinPoint.getArgs();
        return String.valueOf(args[args.length - 1]);
    }
}
