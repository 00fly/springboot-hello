package com.fly.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * knife4j
 *
 * @author jack
 */
@Configuration
@EnableKnife4j
@EnableSwagger2WebMvc
@ConditionalOnWebApplication
public class Knife4jConfig
{
    @Value("${knife4j.enable:false}")
    private boolean enable;
    
    @Bean
    Docket defaultApi()
    {
        return new Docket(DocumentationType.SWAGGER_2).enable(enable)
            .apiInfo(apiInfo())
            .groupName("同步接口组")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.fly.hello.web"))
            .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class)) // 同时满足apis多个条件
            // .apis(RequestHandlerSelectors.basePackage("com.fly.hello.web.asyn").negate()) //取反
            // .paths(PathSelectors.regex("/(demo|knife|show|date|file|swagger)/.*")) // 针对RequestMapping
            .paths(PathSelectors.regex("(?!.*async).*")) // url中不包含async
            .build();
    }
    
    @Bean
    Docket asynApi()
    {
        return new Docket(DocumentationType.SWAGGER_2).enable(enable)
            .apiInfo(apiInfo())
            .groupName("异步接口组")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.fly.hello.web.asyn"))
            .paths(PathSelectors.any()) // 针对RequestMapping
            .build();
    }
    
    private ApiInfo apiInfo()
    {
        return new ApiInfoBuilder().title("数据接口API").description("接口文档").termsOfServiceUrl("http://00fly.online/").version("1.0.0").build();
    }
    
}
