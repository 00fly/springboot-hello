package com.fly.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * 注册WebClientConfig
 */
@Configuration
public class WebClientConfig
{
    @Bean
    WebClient webClient()
    {
        return WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    }
}
