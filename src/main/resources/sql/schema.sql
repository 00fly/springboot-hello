CREATE DATABASE IF NOT EXISTS `hello`;

USE `hello`;

DROP TABLE IF EXISTS `boot_log`;
CREATE TABLE IF NOT EXISTS boot_log ( 
  `id`  bigint NOT NULL AUTO_INCREMENT ,
  `event_id` varchar(50) ,
  `event_date` datetime ,
  `thread` varchar(255) ,
  `class` varchar(255) ,
  `function` varchar(255) ,
  `message` varchar(255) ,
  `exception` text,
  `level` varchar(255) ,
  `time` datetime,
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS t_schedule_lock (
`id`  bigint NOT NULL AUTO_INCREMENT ,
`key_name`  varchar(20) NOT NULL ,
`time`  datetime NOT NULL ,
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS t_run_info (
`id`  bigint NOT NULL AUTO_INCREMENT ,
`key_name`   varchar(20) NOT NULL ,
`key_value`  varchar(20) NOT NULL ,
PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
