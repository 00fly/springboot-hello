package com.fly.core.base;

import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.ResourceUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileListTest
{
    static Resource[] resources = {};
    
    @BeforeAll
    public static void init()
    {
        try
        {
            resources = new PathMatchingResourcePatternResolver().getResources(ResourceUtils.CLASSPATH_URL_PREFIX + "data/pic/**/*.jpg");
            if (resources.length < 4)
            {
                log.error("############### 请在[resources/data/pic/]目录放入不少于4张jpg图片 ###############");
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void browserFiles()
    {
        Stream.of(resources).forEach(r -> {
            try
            {
                log.info("{}", r.getURL().getPath());
            }
            catch (IOException e)
            {
                log.error(e.getMessage(), e);
            }
        });
    }
}
