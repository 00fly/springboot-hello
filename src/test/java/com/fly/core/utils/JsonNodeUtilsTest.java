package com.fly.core.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fly.core.utils.jackson.JsonNodeUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * JsonNodeUtils测试
 */
@Slf4j
public class JsonNodeUtilsTest
{
    static String jsonText;
    
    @BeforeAll
    public static void init()
    {
        Resource resource = new ClassPathResource("data.json");
        try (InputStream input = resource.getInputStream())
        {
            jsonText = IOUtils.toString(input, StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void jsonToPropText()
        throws IOException
    {
        String propText = JsonNodeUtils.jsonNodeToPropsText(JsonNodeUtils.jsonToJsonNode(jsonText));
        log.info("jsonToPropText: {}", propText);
    }
    
    @Test
    public void jsonToXml()
        throws IOException
    {
        String xml = JsonNodeUtils.jsonNodeToXml(JsonNodeUtils.jsonToJsonNode(jsonText));
        log.info("jsonToXML: {}", xml);
    }
    
    @Test
    public void jsonToYaml()
        throws IOException
    {
        String yaml = JsonNodeUtils.jsonNodeToYaml(JsonNodeUtils.jsonToJsonNode(jsonText));
        log.info("jsonToYaml: {}", yaml);
    }
}
