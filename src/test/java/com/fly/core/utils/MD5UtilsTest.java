package com.fly.core.utils;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MD5UtilsTest
{
    @Test
    public void test()
        throws IOException
    {
        // 字符串MD5
        String str = "Marydon";
        String md51 = MD5Utils.encryptToMd5(str);
        String md52 = MD5Utils.encrypt2ToMd5(str);
        log.info("MD5加密方法一：{}", md51);
        log.info("MD5加密方法二：{}", md52);
        
        // 文件MD5
        Resource resource = new ClassPathResource("data/pic/001.jpg");
        md51 = MD5Utils.encryptToMd5(resource.getFile());
        md52 = MD5Utils.encrypt2ToMd5(resource.getFile());
        log.info("MD5加密方法一：{}", md51);
        log.info("MD5加密方法二：{}", md52);
    }
}
