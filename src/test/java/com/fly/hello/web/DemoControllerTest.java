package com.fly.hello.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.fly.HelloApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = HelloApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class DemoControllerTest
{
    @Autowired
    MockMvc mockMvc;
    
    @Test
    public void testFirst()
        throws Exception
    {
        RequestBuilder request = MockMvcRequestBuilders.get("/demo/first").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);
        MvcResult mvcResult = mockMvc.perform(request).andDo(MockMvcResultHandlers.print()).andReturn();
        log.info("★★★★ Response  = {}", mvcResult.getResponse().getContentAsString());
    }
}
