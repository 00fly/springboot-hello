package com.fly.hello.template;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

import com.fly.hello.template.bean.StudentVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@SpringBootTest
public class JdbcTemplateTest
{
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    RowMapper<StudentVO> rowMapper = new BeanPropertyRowMapper<>(StudentVO.class);
    
    @BeforeEach
    public void init()
    {
        // execute可以执行所有SQL语句，因为没有返回值，一般用于执行DDL语句
        jdbcTemplate.execute("drop table if exists student");
        jdbcTemplate.execute("create table student(id int not null,name varchar(50),primary key(id))");
        jdbcTemplate.execute("insert into student(id, name) values(1,'Jack')");
        jdbcTemplate.execute("insert into student(id, name) values(2,'Phil')");
        jdbcTemplate.execute("insert into student(id, name) values(3,'Jenny')");
        jdbcTemplate.execute("delete from student where id > 100");
        log.info("::: init success!!");
    }
    
    @Test
    public void testUpdate()
    {
        // update方法进行增删改
        int count = jdbcTemplate.update("insert into student(id, name) values(?,?)", 4, RandomStringUtils.randomAlphabetic(5));
        log.info("{}", count);
    }
    
    @Test
    public void testQuery()
    {
        // 读取单个对象
        StudentVO studentVO = jdbcTemplate.queryForObject("select id,name from student where id=?", rowMapper, 1);
        log.info("{}", studentVO);
        
        // 读取多个对象
        List<StudentVO> list = jdbcTemplate.query("select id,name from student", rowMapper);
        log.info("{}", list);
        
        // 返回 List<Map<String, Object>>
        List<Map<String, Object>> data = jdbcTemplate.queryForList("select id,name from student");
        log.info("{}", data);
        
        // 返回 Map<String, Object>
        Map<String, Object> map = jdbcTemplate.queryForMap("select * from student where id=?", 2);
        log.info("{}", map);
        
        // 返回指定类型
        int count = jdbcTemplate.queryForObject("select count(*) from student", Integer.class);
        log.info("{}", count);
        
        String name = jdbcTemplate.queryForObject("select name from student where id=?", String.class, 2);
        log.info("{}", name);
    }
    
    @Test
    public void testLike()
    {
        log.info("{}", jdbcTemplate.queryForList("select * from student where name like ?", "%小%"));
        log.info("{}", jdbcTemplate.queryForList("select * from student where name like concat('%', ?, '%')", "小"));
    }
    
    @Test
    public void testBatchUpdate()
    {
        String sql = "insert into student(id, name) values(?, ?)";
        List<Object[]> batchArgs = new ArrayList<Object[]>();
        batchArgs.add(new Object[] {10, "大马"});
        batchArgs.add(new Object[] {11, "小马"});
        batchArgs.add(new Object[] {12, "小刘"});
        batchArgs.add(new Object[] {13, "大刘"});
        batchArgs.add(new Object[] {14, "小强"});
        batchArgs.add(new Object[] {15, "大强"});
        jdbcTemplate.batchUpdate(sql, batchArgs);
    }
}
