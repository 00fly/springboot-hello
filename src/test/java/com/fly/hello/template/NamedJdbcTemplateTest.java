package com.fly.hello.template;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.fly.hello.template.bean.Student;
import com.fly.hello.template.bean.StudentVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@SpringBootTest
public class NamedJdbcTemplateTest
{
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    
    @BeforeEach
    public void init()
    {
        /****** JdbcOperations实际就是JdbcTemplate ******/
        JdbcOperations jdbcOperations = namedJdbcTemplate.getJdbcOperations();
        
        jdbcOperations.execute("drop table if exists student");
        jdbcOperations.execute("create table student(id int not null AUTO_INCREMENT, name varchar(50), create_date datetime, primary key(id))");
        List<Object[]> batchArgs = Arrays.asList(new Object[][] {{"Jack"}, {"Phil"}, {"Jenny"}});
        jdbcOperations.batchUpdate("insert into student(name, create_date) values(?, CURRENT_TIMESTAMP())", batchArgs);
        jdbcOperations.execute("delete from student where id > 100");
        log.info("before::: init success!!");
        log.info("before: {}", namedJdbcTemplate.getJdbcTemplate().queryForObject("select count(*) from student", Long.class));
    }
    
    @AfterEach
    public void after()
    {
        log.info("after: {}", namedJdbcTemplate.getJdbcTemplate().queryForObject("select count(*) from student", Long.class));
    }
    
    @Test
    public void testUpdate()
    {
        // update方法进行增删改-写法1
        String time = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
        Map<String, Object> params = new HashMap<>();
        params.put("name", RandomStringUtils.randomAlphabetic(5));
        params.put("time", time);
        
        String sql = "insert into student(name, create_date) values(:name, :time)";
        int count = namedJdbcTemplate.update(sql, params);
        log.info("update count: {}", count);
        
        // update方法进行增删改-写法2
        count = namedJdbcTemplate.update(sql, new MapSqlParameterSource("name", RandomStringUtils.randomAlphabetic(5)).addValue("time", time));
        log.info("update count: {}", count);
        
        // update方法进行增删改-写法3
        count = namedJdbcTemplate.update(sql, new BeanPropertySqlParameterSource(new StudentVO().setName(RandomStringUtils.randomAlphabetic(5)).setTime(new Date())));
        log.info("update count: {}", count);
    }
    
    @Test
    public void testQueryPk()
    {
        // 查询主键
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedJdbcTemplate.update("insert into student(name, create_date) values(:name, :time)", new BeanPropertySqlParameterSource(new StudentVO().setName(RandomStringUtils.randomAlphabetic(5)).setTime(new Date())), keyHolder);
        long pk = keyHolder.getKey().longValue();
        log.info("pk: {}", pk);
    }
    
    @Test
    public void testQuerySingle()
    {
        log.info("------ all data: {}", namedJdbcTemplate.queryForList("select id, name from student", Collections.emptyMap()));
        
        // 单行单列数据
        Integer count = namedJdbcTemplate.queryForObject("select count(*) from student", new HashMap<>(), Integer.class);
        log.info("count: {}", count);
        count = namedJdbcTemplate.queryForObject("select count(*) from student", EmptySqlParameterSource.INSTANCE, Integer.class);
        log.info("count: {}", count);
        String name = namedJdbcTemplate.queryForObject("select name from student limit 1", EmptySqlParameterSource.INSTANCE, String.class);
        log.info("name: {}", name);
        
        // 单行对象数据
        Student student = namedJdbcTemplate.queryForObject("select id, name from student where id=:id", Collections.singletonMap("id", 1L), new BeanPropertyRowMapper<>(Student.class));
        log.info("student: {}", student);
        
        student = namedJdbcTemplate.queryForObject("select id, name from student where id=:id", new BeanPropertySqlParameterSource(new StudentVO().setId(1L)), new BeanPropertyRowMapper<>(Student.class));
        log.info("student: {}", student);
        
        student = namedJdbcTemplate.queryForObject("select id, name from student limit 1", EmptySqlParameterSource.INSTANCE, new BeanPropertyRowMapper<>(Student.class));
        log.info("student: {}", student);
    }
    
    @Test
    public void testQueryInLike()
    {
        // IN条件
        List<Map<String, Object>> list = namedJdbcTemplate.queryForList("select id, name from student where id in (:ids)", Collections.singletonMap("ids", Arrays.asList(1, 2, 3, 4, 5)));
        log.info("in {}", list);
        
        List<Student> students = namedJdbcTemplate.query("select id, name from student where id in (:ids)", Collections.singletonMap("ids", Arrays.asList(1, 2, 3, 4, 5)), new BeanPropertyRowMapper<>(Student.class));
        log.info("in {}", students);
        
        // like
        log.info("----- like -----");
        log.info("like1: {}", namedJdbcTemplate.queryForList("select * from student where name like :name", Collections.singletonMap("name", "J%")));
        log.info("like2: {}", namedJdbcTemplate.queryForList("select * from student where name like concat('%', :name, '%')", Collections.singletonMap("name", "J%")));
        log.info("like3: {}", namedJdbcTemplate.query("select * from student where name like :name", Collections.singletonMap("name", "J%"), new BeanPropertyRowMapper<>(Student.class)));
    }
    
    @Test
    public void testQueryList()
    {
        log.info("----- List -----");
        
        // 多行单列数据
        List<String> namelist = namedJdbcTemplate.queryForList("select name from student", new HashMap<>(), String.class);
        log.info("namelist: {}", namelist);
        
        // 多行对象数据---Map封装、MapSqlParameterSource封装条件
        List<Student> students = namedJdbcTemplate.query("select * from student where name like :name", Collections.singletonMap("name", "J%"), new BeanPropertyRowMapper<>(Student.class));
        log.info("list 1: {}", students);
        
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource("name", "J%");
        students = namedJdbcTemplate.query("select * from student where name like :name", sqlParameterSource, new BeanPropertyRowMapper<>(Student.class));
        log.info("list 2: {}", students);
    }
    
    @Test
    public void testBatchUpdate()
    {
        String sql = "insert into student(id, name) values(:id, :name)";
        List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
        batchArgs.add(new MapSqlParameterSource("id", 10).addValue("name", "小马"));
        batchArgs.add(new MapSqlParameterSource("id", 11).addValue("name", "大马"));
        batchArgs.add(new MapSqlParameterSource("id", 12).addValue("name", "小刘"));
        batchArgs.add(new MapSqlParameterSource("id", 13).addValue("name", "大刘"));
        batchArgs.add(new MapSqlParameterSource("id", 14).addValue("name", "小强"));
        batchArgs.add(new MapSqlParameterSource("id", 15).addValue("name", "大强"));
        namedJdbcTemplate.batchUpdate(sql, batchArgs.toArray(new SqlParameterSource[0]));
    }
}
