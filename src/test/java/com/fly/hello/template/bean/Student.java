package com.fly.hello.template.bean;

import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Student
{
    private Long id;
    
    private String name;
    
    private Date createDate;
}
