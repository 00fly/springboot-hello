package com.fly.hello.simple.input;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fly.core.utils.jackson.PropertiesUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TemplateTest
{
    @Test
    public void test1()
        throws Exception
    {
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                Properties properties = new Properties();
                properties.put("k1", "v1");
                properties.put("k2", "v2");
                properties.put("k3", "v3");
                log.info("{}", PropertiesUtils.propertiesToJson(properties));
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
    }
    
    @Test
    public void test2()
        throws IOException
    {
        Resource resource = new ClassPathResource("data/bak/data.json");
        try (InputStream input = resource.getInputStream())
        {
            String text = IOUtils.toString(input, StandardCharsets.UTF_8);
            log.info("{}", text);
            log.info("{}", StringEscapeUtils.escapeJson(text));
            text = text.replace("\"", "\\\"");
            log.info("{}", text);
        }
    }
    
}