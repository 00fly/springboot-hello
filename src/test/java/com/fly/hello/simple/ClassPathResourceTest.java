package com.fly.hello.simple;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.ResourceUtils;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClassPathResourceTest
{
    @Test
    public void test()
        throws IOException
    {
        if (ResourceUtils.isFileURL(ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX)))
        {
            File src = new ClassPathResource("application.properties").getFile();
            String path = ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX).getPath() + "log-jdbc.properties";
            File dest = new File(path);
            log.info("{} ==> {}", src.getCanonicalPath(), dest.getCanonicalPath());
            FileUtils.copyFile(src, dest);
        }
    }
    
    @Test
    public void test2()
        throws Exception
    {
        // 通过properties文件传递数据源配置
        if (ResourceUtils.isFileURL(ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX)))
        {
            String path = ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX).getPath() + "jdbc-log.properties";
            Collection<String> lines = new ArrayList<>();
            lines.add("druid.url=");
            lines.add("druid.username=");
            lines.add("druid.password=");
            lines.add("druid.driverClassName=");
            try (OutputStream outputStream = new FileOutputStream(new File(path)))
            {
                IOUtils.writeLines(lines, null, outputStream, StandardCharsets.UTF_8);
            }
            
            // 读取
            Resource resource = new ClassPathResource("jdbc-log.properties");
            Properties properties = PropertiesLoaderUtils.loadProperties(resource);
            DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);
            log.info("properties: {}", properties);
            log.info("dataSource: {}", dataSource);
        }
    }
    
    protected void run()
    {
        String[] args = new String[] {"--spring.config.location=C:\\application-location.yml"};
        // args = new String[] {"-Dspring.config.location=C:\\application-location.yml"};
        if (args.length > 0)
        {
            Stream.of(args).forEach(log::info);
            String location = Stream.of(args).filter(arg -> arg.contains("spring.config.location")).map(arg -> StringUtils.substringAfter(arg, "=")).collect(Collectors.joining());
            log.info("{}", location);
        }
    }
}
