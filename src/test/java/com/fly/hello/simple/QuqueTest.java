package com.fly.hello.simple;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QuqueTest
{
    /**
     * FIFO
     * 
     * @throws InterruptedException
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test()
        throws InterruptedException
    {
        Queue<Integer> quque = new ConcurrentLinkedQueue<>();
        while (true)
        {
            if (quque.size() >= 5)
            {
                // returns null if this queue is empty
                quque.poll();
                
                // NoSuchElementException if this queue is empty
                // quque.remove();
            }
            int input;
            do
            {
                input = RandomUtils.nextInt(0, 10);
            } while (quque.contains(input));
            quque.offer(input);
            log.info("quque: {} <=== {}", quque, input);
            TimeUnit.SECONDS.sleep(5);
        }
    }
    
    /**
     * circleQuque 环形队列
     * 
     * @throws InterruptedException
     */
    @Test
    public void circleQuque()
        throws InterruptedException
    {
        Queue<Integer> quque = new ConcurrentLinkedQueue<>();
        quque.add(600);
        quque.add(500);
        quque.add(400);
        quque.add(300);
        quque.add(200);
        quque.add(100);
        quque.add(0);
        log.info(" {}", quque);
        
        while (!quque.isEmpty())
        {
            Integer out = quque.poll();
            quque.offer(out);
            log.info(" --------------> {}", out);
            log.info(" {} <-- {}\n", quque, out);
            TimeUnit.SECONDS.sleep(5);
        }
    }
}
