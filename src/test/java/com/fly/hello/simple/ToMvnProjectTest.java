package com.fly.hello.simple;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ToMvnProjectTest
{
    /**
     * 转mvn工程
     * 
     * @throws IOException
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void autoResources()
    {
        File tFile = new File("D:\\gitee\\spring_mybatis\\codes\\");
        Set<String> filePaths = new TreeSet<>();
        Collection<File> listFiles = FileUtils.listFilesAndDirs(tFile, FileFilterUtils.suffixFileFilter("java"), DirectoryFileFilter.INSTANCE);
        listFiles.stream().filter(f -> f.isDirectory() && f.getAbsolutePath().endsWith("src\\main")).collect(Collectors.toSet()).forEach(f -> {
            filePaths.add(f.getAbsolutePath());
            new File(f.getAbsolutePath() + "\\resources\\").mkdirs();
        });
        filePaths.forEach(log::info);
    }
    
    /**
     * 转mvn工程
     * 
     * @throws IOException
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void autoMvn()
        throws IOException
    {
        File tFile = new File("c:\\gitee\\spring_mybatis\\codes\\");
        Set<String> filePaths = new TreeSet<>();
        Collection<File> listFiles = FileUtils.listFilesAndDirs(tFile, FileFilterUtils.suffixFileFilter("java"), DirectoryFileFilter.INSTANCE);
        // Stream.of(listFiles).forEach(System.out::println);
        
        listFiles.stream().filter(f -> f.isDirectory()).collect(Collectors.toSet()).forEach(f -> {
            filePaths.add(f.getAbsolutePath());
        });
        for (String path : filePaths)
        {
            if (path.endsWith("src"))
            {
                FileUtils.copyDirectory(new File(path), new File(path.replace("codes", "codes_bak") + "\\main\\java"));
                
                String projectName = StringUtils.substringBetween(path, "codes\\", "\\src").replace("\\", "-");
                String content = IOUtils.toString(new ClassPathResource("pom.xml").getInputStream(), StandardCharsets.UTF_8);
                FileUtils.writeStringToFile(new File(path.replace("codes", "codes_bak") + "\\..\\pom.xml"), String.format(content, projectName), StandardCharsets.UTF_8);
            }
            else if (path.endsWith("WebContent"))
            {
                FileUtils.copyDirectory(new File(path), new File(path.replace("codes", "codes_bak").replace("WebContent", "src") + "\\main\\webapp"));
            }
        }
        Runtime.getRuntime().exec("cmd /c start C:/gitee/spring_mybatis/codes_bak");
    }
    
    /**
     * 转mvn工程准备
     * 
     * @see [类、类#方法、类#成员]
     */
    // @Test
    public void readyMvn()
    {
        String suffixs = "java,jpg,js,jsp,properties,sql,xml";
        File tFile = new File("D:\\gitee\\spring_mybatis\\codes\\");
        Set<String> filePaths = new TreeSet<>();
        Collection<File> listFiles = FileUtils.listFiles(tFile, null, true);
        listFiles.stream().filter(f -> f.getName().contains(".")).collect(Collectors.toSet()).forEach(f -> {
            String suffix = StringUtils.substringAfterLast(f.getName(), ".");
            if (suffixs.contains(suffix))
            {
                filePaths.add(StringUtils.substringAfter(f.getAbsolutePath(), "\\codes\\"));
            }
        });
        filePaths.forEach(log::info);
        
        // 项目-文件
        Map<String, List<String>> map = new TreeMap<>();
        filePaths.forEach(p -> {
            String projectName = p.contains("\\WebContent\\") ? StringUtils.substringBefore(p, "\\WebContent\\") : StringUtils.substringBefore(p, "\\src\\");
            if (!map.containsKey(projectName))
            {
                map.put(projectName, new ArrayList<String>());
            }
            map.get(projectName).add(p);
        });
        map.keySet().forEach(key -> {
            log.info("{} ---> {}", key, map.get(key));
        });
    }
}
