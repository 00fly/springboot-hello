package com.fly.hello.resful;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.fly.HelloApplication;

import lombok.extern.slf4j.Slf4j;

/**
 * MockMvcTest
 * 
 * @author 00fly
 *
 */
@Slf4j
@AutoConfigureMockMvc
@SpringBootTest(classes = HelloApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class MockMvcTest
{
    @Autowired
    MockMvc mockMvc;
    
    @BeforeAll
    public static void setup()
    {
        log.info("★★★★★★★★ @Before");
    }
    
    /**
     * 测试 RestAPI
     * 
     * @throws Exception
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void testRestApi()
        throws Exception
    {
        // get、post
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/show/info").param("pageNo", "1").param("pageSize", "5")).andDo(MockMvcResultHandlers.print()).andReturn();
        log.info("★★★★ Response  = {}", mvcResult.getResponse().getContentAsString());
        
        mockMvc.perform(MockMvcRequestBuilders.post("/").param("pageNo", "1").param("pageSize", "10")).andDo(MockMvcResultHandlers.print());
    }
}
