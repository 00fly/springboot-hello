package com.fly.hello.service;

import java.util.Scanner;
import java.util.stream.IntStream;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;

import com.fly.hello.common.RandomIndex;

import lombok.extern.slf4j.Slf4j;

/**
 * 随机索引应用场景：从一批图片随机连续读取不重复的图片数据<br>
 */
@Slf4j
public class RandomIndexTest
{
    @Test
    public void test()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                // 取值范围[20,40),连续获取次数[20,100)
                int maxNum = RandomUtils.nextInt(20, 40);
                int count = RandomUtils.nextInt(20, 100);
                log.info("###### maxNum: {}, count: {}", maxNum, count);
                RandomIndex randomIndex = new RandomIndex(maxNum);
                IntStream.range(0, count).forEach(i -> randomIndex.getIndex());
                
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
    }
}
