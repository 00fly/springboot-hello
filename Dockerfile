#基础镜像
#FROM openjdk:8-jre-alpine
FROM adoptopenjdk/openjdk8-openj9:alpine-slim

COPY docker/wait-for.sh /
RUN chmod +x /wait-for.sh && \
    ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo 'Asia/Shanghai' >/etc/timezone

#拷贝lib
#RUN本质是在镜像中执行命令，故下面命令判断条件无效
#RUN if [ -d "target/libs/" ]; then COPY target/libs/*.jar /libs/; fi

#拷贝发布包
COPY target/*.jar  /app.jar

EXPOSE 8080

CMD ["--server.port=8080"]

#启动脚本
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-Xshareclasses", "-Xquickstart", "-jar", "/app.jar"]
